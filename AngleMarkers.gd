extends Node2D

class_name AngleMarkers

export(float) var radius = 100.0
export(Color) var color = Color.red
export(float) var marker_length = 10.0
export(float) var marker_width = 2.0
export(int) var num_divisions = 32
export(float) var text_position = 10.0

onready var letter_res = preload("res://Letter.tscn")

func update_markers():
	for n in get_children():
		n.queue_free()
	
	iter_angles("_draw_letters", self, -0.5)
	
	update()
	
func iter_angles(fn_name, dst_object: Object = null, angle_offset = 0):
	var delta_angle = TAU / num_divisions
	
	if dst_object == null:
		dst_object = self
	
	for i in range(num_divisions):
		var angle = (delta_angle * i) + (angle_offset * delta_angle)
		var unit_vec = Vector2(cos(angle), sin(angle))
		
		dst_object.call(fn_name, unit_vec, i, angle)

func _draw():
	iter_angles("_draw_division")

func _draw_letters(unit_vec, i, angle):
	var letter_radius = radius - text_position
	var center = transform.get_origin()
	var outer_vec = unit_vec * letter_radius
	var outer_point = center + outer_vec
	
	var letter = letter_res.instance()
	letter.transform.origin = outer_point
	letter.rotation = angle
	letter.letter = "%X" % i
	letter.color = color.lightened(.5)
	add_child(letter)
		
func _draw_division(unit_vec, i, angle):
	var inner_radius = radius - marker_length
	var center = transform.get_origin()
	var inner_vec = unit_vec * inner_radius
	var outer_vec = unit_vec * radius
	
	var inner_point = center + inner_vec
	var outer_point = center + outer_vec
	
	draw_line(inner_point, outer_point, color, marker_width)
