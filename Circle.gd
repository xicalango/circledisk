extends Node2D

export(float) var radius = 100.0
export(Color) var color = Color.white

export(float) var inner_radius = 20.0
export(Color) var inner_color = Color.red

func _draw():
	var center = transform.get_origin()
	draw_circle(center, radius, color)
	
	draw_arc(center, inner_radius, 0, TAU, 32, inner_color)

	draw_circle(center, 2.0, inner_color)
