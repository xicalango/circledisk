extends Node2D

export(String) var letter = "A"
export(Color) var color = Color.blue

onready var font = Control.new().get_font("font")

func _draw():
	draw_string(font, Vector2(0, 0), letter, color)
