extends Node2D

export(float) var scale_factor = 270/60

func _ready():
	$CircleWithMarker.transform.origin = get_viewport_rect().get_center()
	_on_CircleRadiusSlider_value_changed($UI/GridContainer/CircleRadiusSlider.value)
	$CircleWithMarker/AngleMarkers.update_markers()
	$UI/GridContainer/CircleColorButton.color = $CircleWithMarker/Circle.color
	$UI/GridContainer/MarkerColorButton.color = $CircleWithMarker/AngleMarkers.color

	_on_MarkerLengthSlider_value_changed($UI/GridContainer/MarkerLengthSlider.value)
	_on_TextPositionSlider_value_changed($UI/GridContainer/TextPositionSlider.value)

func _get_radius(value: float):
	return value * scale_factor

func _on_CircleRadiusSlider_value_changed(value):
	var radius = _get_radius(value)
	$CircleWithMarker/Circle.radius = radius
	$CircleWithMarker/AngleMarkers.radius = radius
	
	$CircleWithMarker/Circle.update()
	$CircleWithMarker/AngleMarkers.update_markers()

func _on_DivisionsSlider_value_changed(value):
	$CircleWithMarker/AngleMarkers.num_divisions = int(value)
	$CircleWithMarker/AngleMarkers.update_markers()

func _on_MarkerLengthSlider_value_changed(value):
	$CircleWithMarker/AngleMarkers.marker_length = value * scale_factor
	$CircleWithMarker/AngleMarkers.update_markers()

func _on_CircleColorButton_color_changed(color):
	$CircleWithMarker/Circle.color = color
	$CircleWithMarker/Circle.update()

func _on_MarkerColorButton_color_changed(color):
	$CircleWithMarker/Circle.inner_color = color
	$CircleWithMarker/AngleMarkers.color = color
	$CircleWithMarker/Circle.update()
	$CircleWithMarker/AngleMarkers.update_markers()

func _on_CenterCircleRadiusSlider_value_changed(value):
	$CircleWithMarker/Circle.inner_radius = _get_radius(value)
	$CircleWithMarker/Circle.update()

func _on_TextPositionSlider_value_changed(value):
	$CircleWithMarker/AngleMarkers.text_position = value * scale_factor
	$CircleWithMarker/AngleMarkers.update_markers()

func _on_ExportButton_pressed():
	var content = _build_svg()
	
	var file = File.new()
	file.open("user://export.svg", File.WRITE)
	file.store_string(content)
	file.close()

func _on_ExportButton2_pressed():
	OS.clipboard = _build_svg()
	
func _build_svg():
	var builder = SvgBuilder.new()
	builder.angle_markers = $CircleWithMarker/AngleMarkers
	builder.radius = $UI/GridContainer/CircleRadiusSlider.value
	builder.center_circle_radius = $UI/GridContainer/CenterCircleRadiusSlider.value
	builder.marker_length = $UI/GridContainer/MarkerLengthSlider.value
	builder.text_position = $UI/GridContainer/TextPositionSlider.value
	
	return builder.build_svg()
