extends Node

class_name SvgBuilder

var builder = PoolStringArray()

var angle_markers: AngleMarkers

var radius: float
var center_circle_radius: float
var marker_length: float
var text_position: float

func build_svg():
	
	var values = {
		"circle_radius": radius,
		"circle_diameter": radius * 2,
		"center_circle_radius": center_circle_radius
	}
	
	builder.append('<?xml version="1.0" encoding="iso-8859-1"?>')
	builder.append("""<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 {circle_diameter} {circle_diameter}" width="{circle_diameter}mm" height="{circle_diameter}mm">
	<style>
	  .small {
		font: 1mm sans-serif
	  }
	</style>
	
	<circle cx="{circle_radius}" cy="{circle_radius}" r="{circle_radius}" fill="none" stroke="black" />
	<circle cx="{circle_radius}" cy="{circle_radius}" r="{center_circle_radius}" fill="none" stroke="black" stroke-dasharray="2 1" />
	<circle cx="{circle_radius}" cy="{circle_radius}" r="1px" fill="black" stroke="none" />
	""".format(values)
	)
	
	angle_markers.iter_angles("_append_marker_line", self)
	builder.append("")
	angle_markers.iter_angles("_append_marker_text", self, -0.4)
	
	builder.append("</svg>")
	
	print(angle_markers.marker_length)
	print(angle_markers.text_position)
	return builder.join("\n")

func _append_marker_line(unit_vec, i, angle):

	var inner_radius = radius - marker_length
	var center = Vector2(radius, radius)
	var inner_vec = unit_vec * inner_radius
	var outer_vec = unit_vec * radius
	
	var inner_point = center + inner_vec
	var outer_point = center + outer_vec
	
	var values = {
		"x1": inner_point.x,
		"y1": inner_point.y,
		"x2": outer_point.x,
		"y2": outer_point.y,
		"stroke": str(angle_markers.color)
	}
	
	builder.append('  <line x1="{x1}" y1="{y1}" x2="{x2}" y2="{y2}" stroke="black" stroke-dasharray="2 1" />'.format(values))

func _append_marker_text(unit_vec, i, angle):

	var letter_radius = radius - text_position
	var center = Vector2(radius, radius)
	var outer_vec = unit_vec * letter_radius
	var outer_point = center + outer_vec
	
	var letter = "%X" % i
	
	var values = {
		"x": outer_point.x,
		"y": outer_point.y,
		"letter": letter,
		"angle": rad2deg(angle)
	}
	
	builder.append('  <text x="{x}" y="{y}" class="small" transform="rotate({angle} {x} {y})">{letter}</text>'.format(values))
