extends Control

func _ready():
	pass # Replace with function body.


func _process(delta):
	$GridContainer/CircleRadiusValue.text = str($GridContainer/CircleRadiusSlider.value) + " mm"
	$GridContainer/CenterCircleRadiusValue.text = str($GridContainer/CenterCircleRadiusSlider.value) + " mm"
	$GridContainer/DivisionsValue.text = str($GridContainer/DivisionsSlider.value)
	$GridContainer/MarkerLengthValue.text = str($GridContainer/MarkerLengthSlider.value) + " mm"
	$GridContainer/TextPositionValue.text = str($GridContainer/TextPositionSlider.value) + " mm"

